import numpy as np
import os
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import matplotlib
matplotlib.rcParams.update({'font.size': 32})


DATA_DIRECTORY = "data"

def plot_accuracy(epoch_list,train_accuracy_list,test_accuracy_list):
    '''
    It plots training and testing accuracy

    Parameters:
    epoch_list : stores the epoch_no 
    train_accuracy_list : stores train_accuracy for every epoch
    test_accuracy_list : stores test accuracy for every epoch

    Outputs:
    Shows the plot

    '''
    plt.figure(figsize=(40,40))
    plt.plot(epoch_list,train_accuracy_list,label='train_accuracy')
    plt.plot(epoch_list,test_accuracy_list,label='test_accuracy')
    plt.grid()
    plt.legend()
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.xticks(np.arange(min(epoch_list),max(epoch_list)+1,(max(epoch_list)-min(epoch_list))*.1))
    plt.yticks(np.arange(round(min(min(train_accuracy_list),min(test_accuracy_list)),1),1,0.025))
    
    plt.savefig(os.path.join(DATA_DIRECTORY,'accuracy.png'))
    # plt.show()
    plt.close()

def plot_loss(epoch_list,train_loss_list,test_loss_list):
    '''
    It plots training and testing accuracy

    Parameters:
    epoch_list : stores the epoch_no 
    train_accuracy_list : stores train_accuracy for every epoch
    test_accuracy_list : stores test accuracy for every epoch

    Outputs:
    Shows the plot

    '''
    plt.figure(figsize=(40,40))
    plt.plot(epoch_list,train_loss_list,label='train_loss')
    plt.plot(epoch_list,test_loss_list,label='test_loss')
    plt.grid()
    plt.legend()
    plt.xlabel('Epoch',fontsize=32)
    plt.ylabel('Loss',fontsize=32)
    plt.xticks(np.arange(min(epoch_list),max(epoch_list)+1,(max(epoch_list)-min(epoch_list))*.1))
    plt.yticks(np.arange(round(min(min(train_loss_list),min(test_loss_list)),4),round(max(max(train_loss_list),max(test_loss_list)),4),(round(max(max(train_loss_list),max(test_loss_list)),4)-round(min(min(train_loss_list),min(test_loss_list)),4))*.1))
    
    plt.savefig(os.path.join(DATA_DIRECTORY,'loss.png'))
    # plt.show()
    plt.close()

epoch_list = np.load(os.path.join(DATA_DIRECTORY,"epochs.npy"))
train_loss_list = np.load(os.path.join(DATA_DIRECTORY,"training_losses.npy"))
test_loss_list = np.load(os.path.join(DATA_DIRECTORY,"testing_losses.npy"))
train_accuracy_list = np.load(os.path.join(DATA_DIRECTORY,"training_accuracies.npy"))
test_accuracy_list = np.load(os.path.join(DATA_DIRECTORY,"testing_accuracies.npy"))

plot_accuracy(epoch_list,train_accuracy_list,test_accuracy_list)
plot_loss(epoch_list,train_loss_list,test_loss_list)

