import pandas as pd
import numpy as np
import os
import pickle

data_directory = "data"
data_file = "train.csv"

def check_path(directory,file):

    path = os.path.join(data_directory,file)

    if os.path.exists(path):
        print("Success")

        df = pd.read_csv(path)

        return df

    else:

        print("File doesn't exits")

        exit()

def prepare_data(df):

    labels = np.array(df.iloc[:,0])

    print("Labels:",labels.shape)

    features = np.array(df.iloc[:,1:])

    print("Features before reshape:",features.shape)

    # Reshape the features before saving

    features = np.asarray(features).reshape(features.shape[0],28,28)

    print("Features after reshape:",features.shape)

    # Saving features

    with open(os.path.join(data_directory,'features.npy'),'wb') as f:

        np.save(f,features)

    # Saving labels
    with open(os.path.join(data_directory,'labels.npy'),'wb') as f:

        np.save(f,labels)

    

if __name__ == "__main__":       

    df = check_path(data_directory,data_file)

    prepare_data(df)