import torch
import torch.utils.data as data_utils 

import pandas as pd
import numpy as np

import torch

import torch.utils.data as data_utils 

from torch.autograd import Variable

import matplotlib.pyplot as plot

import torch.nn.functional as F


DEVICE = ("cuda:0" if torch.cuda.is_available()  else "cpu")


class MNIST(torch.nn.Module):

    '''
    Class defines the neural network model for mnist digit recognition
    '''
    def __init__(self,input_layer_size,hidden_layer_1_size,hidden_layer_2_size,output_layer_size):
        '''
        Intializes various important variables

        Parameters:
        input_layer_size : No of neurons in the input layer 
        hidden_layer_1_size : No of neurons in the first hidden layer
        hidden_layer_2_size : No of neurons in the second hidden layer
        output_layer_size : No of neurons in the output layer

        Returns:
        None
        '''
        super(MNIST,self).__init__()
        self.cnn_2d_layer_1 = torch.nn.Conv2d(in_channels=1,out_channels=24,kernel_size=5,stride=1)
        self.maxpool2d = torch.nn.MaxPool2d(kernel_size=3,stride=2)
        self.cnn_2d_layer_2 = torch.nn.Conv2d(in_channels=24,out_channels=64,kernel_size=3,stride=1)
        

        self.input_layer = torch.nn.Linear(input_layer_size,hidden_layer_1_size)
        self.hidden_layer_1 = torch.nn.Linear(hidden_layer_1_size,hidden_layer_2_size)
        self.hidden_layer_2 = torch.nn.Linear(hidden_layer_2_size,output_layer_size)
        self.relu = torch.nn.ReLU()
        self.leaky_relu = torch.nn.LeakyReLU()
        self.sigmoid = torch.nn.Sigmoid()
       
        self.dropout_1 = torch.nn.Dropout(0.7)
        self.dropout_2 = torch.nn.Dropout(0.3)
        
    def forward(self,x):
        '''
        Describes the flow of the neural network

        Parameters:
        x: input

        Returns : 
        x: output of the network
        '''
        #Passing through CNN

        x = self.cnn_2d_layer_1(x)
        x = self.relu(x)
        
        x = self.maxpool2d(x)

        x = self.cnn_2d_layer_2(x)
        x = self.relu(x)

        # print(x.data.size())
        (_,C,W,H)=x.data.size()
        
        #Reshaping 2d to 1d
        x=x.view(-1,C*W*H)

        x=self.input_layer(x)
        x=self.relu(x)
        # x=F.dropout(x, p=0.7,training=True)
        x = self.dropout_1(x)
        x=self.hidden_layer_1(x)
        x=self.relu(x)
        # x=F.dropout(x, p=0.3,training=True)
        x = self.dropout_2(x)
        x=self.hidden_layer_2(x)
        
        return x

class NET_REQUIREMENT():
    def __init__(self,batch_size,no_of_epoch,momentum):
        self.batch_size = batch_size
        # self.learning_rate = learning_rate
        self.no_of_epoch = no_of_epoch
        self.momentum = momentum
        
    
    def get_dataloader(self,features,labels):

        try:
            
            train_features = torch.Tensor(features)
            train_labels = torch.LongTensor(labels)

            if DEVICE == "cuda:0":
                train_features = train_features.to(DEVICE)
                train_labels = train_labels.to(DEVICE)
            else:
                pass

            train_data = data_utils.TensorDataset(train_features,train_labels)
            train_data_loader = data_utils.DataLoader(dataset=train_data,batch_size=self.batch_size,shuffle=True,drop_last=True)
        
        except:

            print("data loader not properly working")

        return train_data_loader

    def get_criterion(self):
        return torch.nn.CrossEntropyLoss().cuda()
        
        
    def get_optimizer(self,model,learning_rate=0.001,weight_decay=0):
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
        return optimizer

if __name__ == "__main__":
    pass
    
