import torch
import os
import torch.nn.utils.prune as prune

import numpy as np
from cnn import MNIST
from torch.autograd import Variable

from train_cnn import get_features_labels
import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'font.size': 32})

mnist_net = MNIST(5184,2048,1024,10)

DATA_DIRECTORY = "data"
MODEL_FILE = "digit_recognizer.pth"
BATCH_SIZE = 100


mnist_net.load_state_dict(torch.load(os.path.join(DATA_DIRECTORY,MODEL_FILE)))

def get_accuracy(test_features,test_labels):

    mnist_net.eval()

    total_tested = 0
    correctly_tested = 0

    for i in range(int(test_labels.shape[0]/BATCH_SIZE)):

        t_features = (torch.Tensor(test_features[i*BATCH_SIZE:(i+1)*BATCH_SIZE,:,:,:]))

        t_labels = (torch.LongTensor(test_labels[i*BATCH_SIZE:(i+1)*BATCH_SIZE]))

        t_features = Variable(t_features)
        t_labels = Variable(t_labels)

        output=mnist_net(t_features)

        _ , predicted = torch.max(output.data,1)

        total_tested += len(t_labels)
        
        correctly_tested += (predicted==t_labels.data).sum()

    accuracy = correctly_tested/(total_tested*1.0)

    return accuracy

def plot_accuracies():

    accuracies_conv_1 = np.load(os.path.join(DATA_DIRECTORY,'prune_conv_layer1.npy'))
    accuracies_conv_2 = np.load(os.path.join(DATA_DIRECTORY,'prune_conv_layer2.npy'))
    accuracies_fcl_1 = np.load(os.path.join(DATA_DIRECTORY,'prune_first_1d_layer.npy'))
    accuracies_fcl_2 = np.load(os.path.join(DATA_DIRECTORY,'prune_second_1d_layer.npy'))
    accuracies_fcl_3 = np.load(os.path.join(DATA_DIRECTORY,'prune_third_1d_layer.npy'))

    plt.figure(figsize=(40,40))
    plt.plot(np.arange(0,100,1),accuracies_conv_1,label='pruned 1st cnn layer')
    plt.plot(np.arange(0,100,1),accuracies_conv_2,label='pruned 2nd cnn layer')
    plt.plot(np.arange(0,100,1),accuracies_fcl_1,label='pruned 1st fully connected layer')
    plt.plot(np.arange(0,100,1),accuracies_fcl_2,label='pruned 2nd fully connected layer')
    plt.plot(np.arange(0,100,1),accuracies_fcl_3,label='pruned 3rd fully connected layer')
    plt.grid()
    plt.legend()
    plt.xlabel('Prune Percentage')
    plt.ylabel('Accuracy')
    
    plt.savefig(os.path.join(DATA_DIRECTORY,'pruning_effect.png'))
    # plt.show()
    plt.close()



if __name__ == "__main__":
    
    
    _,test_features,_,test_labels=get_features_labels()

    test_features = test_features[:,np.newaxis,:,:]

    accuracies = []

    for i in range(100):
        mnist_net = MNIST(5184,2048,1024,10)


        mnist_net.load_state_dict(torch.load(os.path.join(DATA_DIRECTORY,MODEL_FILE)))

        # Neural Network module to prune
        module = mnist_net.hidden_layer_2

        prune.random_unstructured(module, name="weight", amount=(i/(100*1.0)))

        accuracy=get_accuracy(test_features,test_labels)
        accuracies.append(accuracy)
        print("Accuracy:",accuracy," Non zero elements:",mnist_net.hidden_layer_2.weight.nonzero().shape)

    '''
    with open(os.path.join(DATA_DIRECTORY,'prune_third_1d_layer.npy'),'wb') as f:

        np.save(f,accuracies)
    '''
    
    torch.save(mnist_net.state_dict(),os.path.join(DATA_DIRECTORY,'pruned_third_1d_layer.pth'))

    plot_accuracies()
    
    

# prune.random_unstructured(module, name="weight", amount=0.3)

# print(module.weight.nonzero().shape)









