import matplotlib.pyplot as plt

import pandas as pd
import numpy as np

import torch

import torch.utils.data as data_utils 

import torch.nn.functional as F

import datetime as dt

import pickle

import os

from torch.autograd import Variable
from sklearn.model_selection import train_test_split

from cnn import MNIST,NET_REQUIREMENT

DATA_DIRECTORY = "data"
FEATURES_FILE = "features.npy"
LABELS_FILE = "labels.npy"
MODEL_FILE = "digit_recognizer.pth"

NO_OF_EPOCH = 31
BATCH_SIZE = 100
MOMENTUM = 0.6



def get_features_labels():

    features = np.load(os.path.join(DATA_DIRECTORY,FEATURES_FILE))
    labels = np.load(os.path.join(DATA_DIRECTORY,LABELS_FILE))

    print(features.shape,labels.shape)
    
    # Split features,label into train and test
    train_features,test_features,train_labels,test_labels = train_test_split(features,labels,train_size=0.8,test_size=0.2,stratify=labels,random_state=1)
    
    return train_features,test_features,train_labels,test_labels
    

def train():
    
    train_features,test_features,train_labels,test_labels = get_features_labels()

    print("Train features:",train_features.shape," Test features:",test_features.shape,test_labels.shape)

    #Added a new axis to make it compatible with 2d Conv i.e (Batch_size,1,height,width)
    train_features = train_features[:,np.newaxis,:,:]
    test_features = test_features[:,np.newaxis,:,:]

    print("Train features after dimension addition:",train_features.shape)

    #Object of the model
    mnist_net = MNIST(5184,2048,1024,10)

    # Get criterion,optimizer,train_data_loader
    obj_net_requirement = NET_REQUIREMENT(BATCH_SIZE,NO_OF_EPOCH,MOMENTUM)
    criterion = obj_net_requirement.get_criterion()
    optimizer = obj_net_requirement.get_optimizer(mnist_net)
    train_data_loader = obj_net_requirement.get_dataloader(train_features,train_labels)

    training_losses = []
    training_accuracies = []
    testing_losses = []
    testing_accuracies = []
    epochs = []

    for epoch in range(NO_OF_EPOCH):
        epochs.append(epoch+1)

        print("Epoch:",epoch)

        training_loss = 0
        correctly_trained = 0
        total_trained = 0

        testing_loss = 0
        correctly_tested = 0
        total_tested = 0

        for i,(train_features,train_labels) in enumerate(train_data_loader):
            
            train_features=Variable(train_features)
            train_labels=Variable(train_labels)

            mnist_net.train()
            
            optimizer.zero_grad()

            output=mnist_net(train_features)
            
            loss = criterion(output,train_labels)

            training_loss += loss.item()

            _ , predicted = torch.max(output.data,1)

            total_trained += len(train_labels)
            
            correctly_trained += (predicted==train_labels.data).sum()

            loss.backward()
            
            optimizer.step()
        
        accuracy = correctly_trained/(total_trained*1.0)
        training_loss = training_loss/(total_trained*1.0)
        training_accuracies.append(accuracy)
        training_losses.append(training_loss)

        print("Training loss:",training_loss," Correctly Trained:",correctly_trained," Total Trained:",\
            total_trained," Train Accuracy:",accuracy)

        mnist_net.eval()

        for i in range(int(test_labels.shape[0]/BATCH_SIZE)):

            t_features = (torch.Tensor(test_features[i*BATCH_SIZE:(i+1)*BATCH_SIZE,:,:,:]))

            t_labels = (torch.LongTensor(test_labels[i*BATCH_SIZE:(i+1)*BATCH_SIZE]))

            t_features = Variable(t_features)
            t_labels = Variable(t_labels)

            output=mnist_net(t_features)
            
            loss = criterion(output,t_labels)

            testing_loss += loss.item()

            _ , predicted = torch.max(output.data,1)

            total_tested += len(t_labels)
            
            correctly_tested += (predicted==t_labels.data).sum()

        accuracy = correctly_tested/(total_tested*1.0)
        testing_loss = testing_loss/(total_tested*1.0)
        testing_losses.append(testing_loss)
        testing_accuracies.append(accuracy)
        
        print("Testing loss:",testing_loss," Correctly Tested:",correctly_tested," Total Tested:",\
            total_tested," Test Accuracy:",accuracy)
    
    # Save model,accuracies,losses,epochs 
    torch.save(mnist_net.state_dict(),os.path.join(DATA_DIRECTORY,MODEL_FILE))

    with open(os.path.join(DATA_DIRECTORY,'training_losses.npy'),'wb') as f:

        np.save(f,training_losses)

    with open(os.path.join(DATA_DIRECTORY,'testing_losses.npy'),'wb') as f:

        np.save(f,testing_losses)

    with open(os.path.join(DATA_DIRECTORY,'training_accuracies.npy'),'wb') as f:

        np.save(f,training_accuracies)

    with open(os.path.join(DATA_DIRECTORY,'testing_accuracies.npy'),'wb') as f:

        np.save(f,testing_accuracies)    
    
    with open(os.path.join(DATA_DIRECTORY,'epochs.npy'),'wb') as f:

        np.save(f,epochs)
    
    print("Training Losses:",training_losses)
    print("Training Accuracies:",training_accuracies)

    print("Testing Losses:",testing_losses)
    print("Testing Accuracies:",testing_accuracies)

if __name__ == "__main__":

    
    train()

